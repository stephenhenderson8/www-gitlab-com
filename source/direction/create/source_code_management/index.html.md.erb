---
layout: markdown_page
title: "Category Direction - Source Code Management"
description: "Source Code Management provides the core workflows and controls for teams to collaborate using Git to build great software. Find more information here!"
canonical_path: "/direction/create/source_code_management/"
---

- TOC
{:toc}

## Source Code Management

**Last updated**: 2021-05-05

| Section | Stage | Maturity | Last Reviewed |
| --- | --- | --- | --- |
| [Dev](/direction/dev/) | [Create](https://about.gitlab.com/stages-devops-lifecycle/create/) | [Loveable](/direction/maturity/) | 2020-12-15 |

## Introduction and how you can help

The Source Code Management direction page belongs to the [Source Code](/handbook/product/categories/#source-code-group) group of the [Create](/stages-devops-lifecycle/create/) stage,
and is maintained by [Sarah Waldner](https://gitlab.com/sarahwaldner), who can be reached at swaldner@gitlab.com.

This direction is a work in progress, and everyone can contribute.
Please comment and contribute in the linked issues and epics.
Sharing your feedback directly on GitLab.com is the best way to contribute to our strategy and vision.

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Asource%20code)
- [Epic List](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Asource%20code)

## Overview

<!--
A good description of what your category is.
If there are special considerations for your strategy or how you plan to prioritize, the description is a great place to include it.
Please include usecases, personas, and user journeys into this section.
-->

Source Code Management provides the core workflows and controls for teams to collaborate using Git to build great software,
including repository view, protected branches, code owners, merge request approvals, and mirroring.

### Performance indicators

The primary performance indicator (PI) for our group is the [number of unique users writing to a project Git repository](https://about.gitlab.com/handbook/product/dev-section-performance-indicators/#createsource-code---gmau---unique-users-writing-to-a-project-git-repository). We want to ensure all the features in our group provide a great experience that ultimately will allow everyone to contribute more often. A great experience in our group is a critical starting point for this.

<embed width="100%" height="400px" src="<%= signed_periscope_url(chart: 11616492, dashboard: 858264, embed: 'v2') %>"> 

Aligning with our SaaS first and product depth direction, we are also working to make performance our secondary indicator (see [related issue](https://gitlab.com/gitlab-org/gitlab/-/issues/327659 )). The intent here is to track the most heavily used services in our group and track how they improve over time. We firmly believe [speed is the killer feature](https://bdickason.com/posts/speed-is-the-killer-feature) and as such will work to provide a speedy experience to set a great stage for new and existing users.

See more detail in the [Create:Source Code PI page section](https://about.gitlab.com/handbook/product/dev-section-performance-indicators/#createsource-code-secondary---other---gitlabcom-performance-scores).

<!-- TODO: public chart visualizing performance (ie Grafana chart) -->

### Target Audience

Source code management targets mainly software engineers but also anyone who is contributing to any types of project. To that end, we target all the [user personas](/handbook/marketing/strategic-marketing/roles-personas/#user-personas) we describe in our handbook, with a special focus on the following:

- **[Sasha (Software Developer):](/handbook/marketing/strategic-marketing/roles-personas/#sasha-software-developer)** targets full time contributors to all types of projects (commercial, OSS, data science, etc.). These users expect and need a high level of reliability and speed in their interactions with both project files and Git.

- **[Delaney (Development Team Lead):](/handbook/marketing/strategic-marketing/roles-personas/#delaney-development-team-lead)** targets users who often times have elevated roles which allow for the management of project settings, such as access control, security, commit strategies, and mirroring.

- **[Devon (DevOps Engineer):](/handbook/marketing/strategic-marketing/roles-personas/#devon-devops-engineer)** targets engineers tasked with supporting and enabling software teams. Their tasks often revolve around platform creation and maintenance, where [GitOps](/topics/gitops/)] workflows are crucial.

### User journeys

The Source Code Management category is expansive and encompasses a broad set of features. Which features are leveraged, how they are leveraged, and to what extent greatly depends on the size of development team and the complexity of the product that they are building. We find that as the team size grows, as does the complexity of the software product. For these reasons, it is challenging to define a single user journey that captures how our users move through Source Code. That being said, there are five main buckets we can use to group the jobs to be done of our users. Understanding this general workflow helps to focus product development and discovery when exploring how to streamline the Source Code experience

![image.png](./source_code_user_journey.png)

We intend to define and document the user journeys our specific personas take and how those differ based on the size of the development and the industry in which they work.

### Connection between Source Code Management and Code Review

The Source Code category of GitLab offers the features where the creative process begins. Here authors will not only consume existing project contents but will also author new content that will eventually move through the DevOps lifecycle. Additionally, many of the features in Source Code are consumed in the Code Review stage of the software developement lifecycle. Consider the following examples:

* [Merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/) allow project maintainers to define **who** is the most relevant user/group to review a particular portion of the codebase. While this feature is consumed in the Code Review process, it is created and managed in the Source Code Management process.
* [Code owners](https://docs.gitlab.com/ee/user/project/code_owners.html) enable teams to present clear signals around codebase ownership as well as enables project maintainers to ensure subject matter experts are reviewing incoming changes. This feature is created and managed in Source Code, but part of its outputs are consumed in Code Review.
    
Because of this close relationship, the Source Code Management group must work closely with the [Code Review group](/direction/create/code_review/) in order to ensure the developer experience is cohesive and efficient. This experience is core to providing a great first impression for users.

<!--
### Challenges to address

- What needs, goals, or jobs to be done do the users have?
- How do users address these challenges today? What products or work-arounds are utilized?

Provide links to UX Research issues, which validate these problems exist.
-->

## Where we are Headed

<!--
Describe the future state for your category.
- What problems are we intending to solve?
- How will GitLab uniquely address them?
- What is the resulting benefits and value to users and their organizations?

Use narrative techniques to paint a picture of how the lives of your users will benefit from using this category once your strategy is at least minimally realized.
-->

Building great software depends on teams working well together.
Teams can rarely be divided into areas of complete independence.
As cross-functional security, compliance and growth teams are formed,
or new services and libraries are created,
effective coordination and collaboration is a must.
This is true whether using a single monolithic repository,
or spread across numerous smaller services and libraries.

Teams require the controls to protect production while making it easy for everyone contribute.
This means providing more granular and dynamic controls so that low risk changes can be made easily,
and only the highest risk changes require the strictest controls.

When building software, teams greatly benefit from using open-source projects and may even submit
contributions upstream. However, the balance of contribution vs. consumption is askew, partly
because of a lack of controlled upstream workflows. Particularly from closed projects.

Upstreaming contributions from private repositories to a public upstream should be simple and safe,
even for conservative organizations.
Whether the upstream repository is on the same GitLab server,
is hosted on GitHub.com,
or managed via a mailing list.

### Critical Product Principles
GitLab maintains a set of [Product Principles](https://about.gitlab.com/handbook/product/product-principles),
some of which are more critical to be aware of in the
Source Code category. Here they are, and why the are critical:
- [Convention over Configuration](https://about.gitlab.com/handbook/product/product-principles/#convention-over-configuration):
Configuring how code is accessed, protected, contributed, and presented varies greatly among software teams.
Striking a balanced set of defaults for new and existing features is paramount to enabling productive teams.
- [Parity between SaaS and Self-managed deployments](https://about.gitlab.com/handbook/product/product-principles/#parity-between-saas-and-self-managed-deployments): Software teams set contribution policies either
globally (enterprise) or by enabling each team to decide what's best for them (OSS, startups). Source Code features
must be flexible enough to serve teams of all sizes, regardless of their environment.
- [Simplicity](https://about.gitlab.com/handbook/product/product-principles/#simplicity): As we add more features,
it's important for users to keep the focus on the applications they are building. We don't want to make users think.
- [Minimum Viable Change (MVC)](https://about.gitlab.com/handbook/product/product-principles/#the-minimal-viable-change-mvc):
The SCM space is an endless source of use-cases and problems that arise from every different style, team type, use-case, etc.
After we've validated a problem, we'll try to solve it with the smallest possible solution and iterate from there.

### What's Next & Why

<!--
This is almost always sourced from the following sections, which describe top priorities for a few stakeholders.
This section must provide a link to an issue or [epic](/handbook/product/product-processes/#epics-for-a-single-iteration) for the MVC or first/next iteration in the category.
-->

- [SaaS first](https://about.gitlab.com/direction/#saas-first)

    GitLab's success has translated into tremendous user growth, both for our self-managed and SaaS offerings.
    While working on supporting scale and performance, we have identified great opportunities to focus on.

    In 2021 we are placing special emphasis on strengthening our SaaS offering by focusing on ensuring
    feature parity with our market leading self-managed offering. For the Source Code group, this means
    focusing on delivering solutions that are scalable, performant, and secure. We will focus on the
    following areas throughout the year:

    - [Performance](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Asource%20code&label_name[]=performance): Ensure GitLab.com performs well at scale as well as provides a great developer experience on key workflows and actions. Focus on resolving existing failure scenarios and technical debt.

    - [Security](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Asource%20code&label_name[]=security): Industry leading security to safeguard user's data.

    - [Infradev](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Asource%20code&label_name[]=infradev): Protect GitLab.com's availability from infrastructure failure.
- [Framework for Source Code Rules](https://gitlab.com/groups/gitlab-org/-/epics/6351)

    GitLab offers a number of controls that can be implemented as safeguards. These controls can be put in place to keep changes from having a negative or enforce adherence to policies. Integrating features like protected branches, approval rules, code owners (approvals) and soon “status checks” should have an experience that easy to set up, maintain, and consume downstream.

- [Improvements to Code owners](https://gitlab.com/groups/gitlab-org/-/epics/5886)

    For large repositories (especially monorepos) code owners files can get quite large. Keeping things organized is harder, especially ensuring that entries in one part of the file will not override entries in another part of the file. Also, readability for new users coming on board is difficult.
Finally, changes to the file by multiple parties can create merge conflicts that need to be reconciled manually between multiple parties. We also know that large organizations with many projects and large projects need to enforce review policies so that they can ensure the correct teams and individuals review changes that impact them. File owners will be automatically added to related Merge Requests (separate feature), but it is also necessary to add controls to prevent changes directly to important branches without approval.  We will be continuing to iterate on the first version of codeowners https://gitlab.com/groups/gitlab-org/-/epics/77. Specifically starting with [supporting multiple codeowners file](https://gitlab.com/groups/gitlab-org/-/epics/5887).

- [Improvements to protected branches](https://gitlab.com/groups/gitlab-org/-/epics/522)

    Sensitive information sometimes accidentally pushed to Git repositories. Although it is possible to safeguard against this in various ways it isn't possible to succeed every time. Sensitive information is a broad definition that could include trade secrets in the form of lab results from testing an experimentally drug, or personal information of a real person that was used to replicate and fix a bug. Unlike passwords, this information can't be rotated and needs to be permanently removed from the repository.
There are tools for removing information from a Git repository like git filter-branch and  https://rtyley.github.io/bfg-repo-cleaner/ but this is incomplete because GitLab uses refs to make sure that the commits in a merge request and diffs that have been commented on are not removed, so that future developers can read discussions about the previous code changes.

- [Improve commit list and details](https://gitlab.com/groups/gitlab-org/-/epics/166)

   Providing relevant metadata for commits allows developers and release managers to determine which merge requests carry the relevant code for a certain change. This is an important part of the release cycle which allows effective packaging and deployment.


### What is Not Planned Right Now

<!--
Often it's just as important to talk about what you're not doing as it is to discuss what you are.
This section should include items that people might hope or think we are working on as part of the category, but aren't, and it should help them understand why that's the case.
Also, thinking through these items can often help you catch something that you should in fact do.
We should limit this to a few items that are at a high enough level so someone with not a lot of detailed information about the product can understand the reasoning.


- [Cross-project merge requests](https://gitlab.com/groups/gitlab-org/-/epics/882)

    Many software products often consists of many services and/of components that are 
    need to be integrated to ship complex applications, but for reasons of scale, access
    control and history, it may not be possible or advisable to have many hundreds of
    projects operating in the same Git repository and the same GitLab project.

    Providing a top-level merge request that in turn actions lower-level merge requests
    in related projects can provide a central point of action to submit, review, approve,
    and merge changes into the main branch.
-->
- [Branch read access controls](https://gitlab.com/gitlab-org/gitlab-ee/issues/720)

    Limiting which branches a user can read in a Git repository is possible in a basic sense,
    by only advertising a subset of refs,
    but it is not possible to guarantee that unreachable objects will not be sent to the client.
    This means that branch read access controls would be very weak,
    since they could not prevent exfiltration of data they do not have permission to read.

- Path-level read access controls

    From a commit, Git expects all trees and blobs to be reachable.
    Although Git supports partial clone and spares checkout,
    which allow data to be excluded from fetch and checkout,
    Git expects to be able to fetch missing objects on demand.
    Deliberately excluding objects by path is likely to cause unexpected failures.

### Maturity Plan

<!--
It's important your users know where you're headed next.
The maturity plan section captures this by showing what's required to achieve the next level.
-->

This category is currently at the **Loveable** maturity level (see our [definitions of maturity levels](https://about.gitlab.com/handbook/product/categories/maturity/#legend)).

However, specific aspects are not yet loveable:

- [Forking](https://gitlab.com/groups/gitlab-org/-/epics/264)
- [Mirroring](https://gitlab.com/groups/gitlab-org/-/epics/852)

## Competitive Landscape

<!--
Lost the top two or three competitors.
What the next one or two items we should work on to displace the competitor at customers?
Ideally these should be discovered through [customer meetings](/handbook/product/product-processes/#customer-meetings).

We’re not aiming for feature parity with competitors,
and we’re not just looking at the features competitors talk about,
but we’re talking with customers about what they actually use,
and ultimately what they need.
-->

Important competitors are [GitHub](https://github.com) and [Perforce](https://perforce.com),
and increasingly [Azure DevOps](https://docs.microsoft.com/en-us/azure/devops/user-guide/alm-devops-features).

For public open source projects, GitHub is our primary competitor,
with millions of active users having chosen GitHub before the first version of GitLab ever existed.

In most source code management capabilities GitLab compares favorably to GitHub,
the most notable exception being the maturity of forking workflows which GitHub pioneered.
GitHub has a highly polished and fast product,
which makes tasks like browsing and managing projects fast and easy.

- [Make forking loveable](https://gitlab.com/groups/gitlab-org/-/epics/264)

For users of [SVN (Apache Subversion)](https://subversion.apache.org/) intending to migrate to Git,
GitHub is a significant competitor, particularly because GitHub supports hosting SVN repositories.

[Perforce](https://perforce.com) competes with GitLab primarily on its ability to support enormous repositories,
however, Perforce also competes on the basis of being a Centralized Version Control System.
This means that Perforce not only supports granular write permissions,
but granular read permissions on a branch and file path basis.
While fine grained read permissions are important to some customers,
large monolithic repositories may be split into smaller repositories allowing read controls and easier management.

- [Git for enormous repositories](https://gitlab.com/groups/gitlab-org/-/epics/773)

## Analyst Landscape

<!--
What are analysts and/or thought leaders in the space talking about?
What are one or two issues that will help us stay relevant from their perspective?
-->

Large file support (see [Gitaly direction](/direction/create/gitaly/)) is an ongoing area of interest because it blocks certain segments of software development from using Git.

Similarly extremely large repository support (see [Gitaly direction](/direction/create/gitaly/)) is also an area of interest for the same reason.

## Top Customer Success/Sales issue(s)

<!--
These can be sourced from the CS/Sales top issue labels when available,
internal surveys, or from your conversations with them.
-->

The most frequent category of request is for improved support for finer grained controls,
so that policies can be enforced at key points in the workflow,
and more permissive permissions can be granted at other times.

- Support for very large repositories (see [Gitaly direction](/direction/create/gitaly/)), requested particularly by customers migrating from a Centralized Version Control System.
- [Improved support for forking workflows](https://gitlab.com/groups/gitlab-org/-/epics/264), which is important for customers large and small, commercial and open source.

## Top user issue(s)

<!--
This is probably the top popular issue from the category (i.e. the one with the most thumbs-up),
but you may have a different item coming out of customer calls.
-->

- [Automatically create tag when merging](https://gitlab.com/gitlab-org/gitlab/-/issues/15962)
- [Clone statistics](https://gitlab.com/gitlab-org/gitlab/-/issues/15807)

## Top dogfooding issues

<!--
These are sourced from internal customers wanting to [dogfood](/handbook/values/#dogfooding) the product.
-->

- [Subscribe to code changes](https://gitlab.com/gitlab-org/gitlab/-/issues/1817)

## Top Vision Item(s)

<!--
What's the most important thing to move your vision forward?
-->

- Support for monolithic repositories (see [Gitaly direction](/direction/create/gitaly/))
- [🎨 Improve file locking for binary asset workflows](https://gitlab.com/groups/gitlab-org/-/epics/1488)
- [Support multiple code owner files](https://gitlab.com/groups/gitlab-org/-/epics/306)
