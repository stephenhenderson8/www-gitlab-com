---
layout: handbook-page-toc
title: "Remote work starter guide for employees: how to adjust to work-from-home"
canonical_path: "/company/culture/all-remote/remote-work-starter-guide/"
description: Remote work starter guide for employees
twitter_image: "/images/opengraph/all-remote.jpg"
twitter_image_alt: "GitLab remote team graphic"
twitter_site: "@gitlab"
twitter_creator: "@gitlab"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Introduction

[COVID-19 (Coronavirus)](https://www.cdc.gov/coronavirus/2019-ncov/index.html) has forced untold employees into a work-from-home arrangement. For those who are accustomed to a dedicated workspace outside of the home, being mandated to work in the same place you sleep is disorienting at best, and debilitating at worst. 

For **employees** who are grappling with their new remote reality, here are five tips to implement straight away in your journey to acclimate. 

*(If you're a **founder or team leader** in need of advice for managing a remote team, view our [employer-focused remote work emergency toolkit](/company/culture/all-remote/remote-work-emergency-plan/).)*

## Carve out a dedicated workspace (achieving focus)

![GitLab all-remote laptop illustration](/images/all-remote/gitlab-all-remote-laptop-map-illustration.jpg){: .shadow.medium.center}

Where you work is as important as what you work on and who you work with. If you're able to use a dedicated space or room purely for work, that is ideal. If not, even a simple curtain to block off a place of work may usher you into a place of focus. The execution of this will look different depending on your home, and who all is in the domicile during your working hours, but the key is to find a space that is purely for work.

Once found, focus next on ergonomics. Ask your employer if they will allow reimbursements on certain items (chairs, noise-canceling headphones, monitors, external keyboards, etc.) which promote a healthy and focused workspace. Try not to compromise on comfort. You may be able to work this way for a week or two, but over time your productivity, health, and mood will likely decline. 

For a more detailed list of considerations, visit GitLab's [guide to a productive home office](/company/culture/all-remote/workspace/).

## Separate work from life (preventing burnout)

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/uP0aiei0JKA" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

*In the [video](https://youtu.be/uP0aiei0JKA) above, GitLab's Head of Remote Darren Murph talks to Sprinklr's [Andrew Kasier](https://www.linkedin.com/in/andrewrkaiser/) about the mental health benefits that can come with remote work and how removing the pressures of an office allows for deep work. Discover more in GitLab's [Remote Work playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0Kq7QUX-Ux5fOunQotqJbECc).*

This is likely to be the most difficult hurdle to clear, particularly for new work-from-home employees who have family in the home around the clock. You should have a dedicated conversation with family, helping them understand that just because you're home, doesn't mean you're available. 

A shortcut to boundary setting is this: "If it's important enough that you'd commute to my usual office and come to my desk, then it's important enough for you to visit my home workspace." You may also consider a [busy/available indicator](/company/culture/all-remote/workspace/#busyavailable-indicators). 

When there's no physical office to leave from, it's tempting to work longer than is expected (or healthy). If useful, set reminders to begin and end work, and pre-plan activities to fill the void where a commute once stood. Proactively planning what you'll do with your commute time is key to ramping into a workday and ramping off. This will look different for each individual, but leaving your home for a walk or a planned activity with friends/community is a great way to create unmistakable separation. 

For more on the topic of mental health, explore GitLab's [guide to combating burnout, isolation, and anxiety in the remote workplace](/company/culture/all-remote/mental-health/).

## Don't stop engaging with people (avoiding loneliness)

![GitLab values illustration](/images/all-remote/gitlab-values-tanukis.jpg){: .medium.center}

When there is no office to influence spontaneous [informal communication](/company/culture/all-remote/informal-communication/), you must be intentional about weaving it into your day. 

* Schedule regular [coffee chats](/company/culture/all-remote/informal-communication/#coffee-chats) with people using a video call.
* Experiment with audio-based chat tools like [Yac](https://www.yac.com/) and video-based tools like [Loom](https://www.loom.com/).
* Create an always-on video conferencing room that your team can work from. (And remember, in a remote setting, [it's OK to look away](/company/culture/all-remote/meetings/#its-ok-to-look-away)!)
* Talk about what you normally would. If sports, vacation plans, and hilarious tales of insubordination by children are common water-cooler material, work with your team to establish a chat channel to discuss things outside of work. The medium may be different, but the connection is the same.
* Drop any shame or embarrassment. Everyone is in the same boat — a forced work-from-home arrangement with no preparation. Don't [worry about your background](/company/culture/all-remote/meetings/#meetings-are-about-the-work-not-the-background), and feel welcome to let your pets and family find their way into calls on occasion. It humanizes the experience and reminds everyone that we're people first, and colleagues second. 
* Connect with family and community. Working from home gives you an opportunity to spend time with a different set of people than just your coworkers. Look for opportunities to build bonds with family and [community](/company/culture/all-remote/people/), which may have been impossible or limited when you had a commute. 

## Respect the routine, but experiment with change (finding structure)

While embracing [asynchronous workflows](/company/culture/all-remote/asynchronous/) is a significant benefit of an [all-remote](/company/culture/all-remote/terminology/) team, temporary work-from-home arrangements may be less amenable to massive swings in time zone adoption. If this is the case, it's wise to formulate a routine that closely aligns with your prior routine. As mentioned above, the key is proactively filling the space that once held your commute. Aim for using this time to make yourself [healthier](/company/culture/all-remote/mental-health/). Exercising, resting, bonding with family, cooking, reading, studying, etc. — all great options. If you aren't careful, that time can be squandered and the lines between sleeping and working are blurred. 

However, do not feel beholden to a routine. A perk of remote is the ability to experiment with unconventional working days. It's understood that not everyone shares the same peak hours of energy and focus. If you feel that you work best in late evenings, for example, have that conversation with your team and experiment with a non-linear workday, a term that describes the splicing of life and work in a deliberate stop-and-start fashion to maximize one's quality of life and work. 

## Roll with the changes (embracing iteration)

Relax: you aren't born knowing how to work from home. Companies built on the expectation of gathering people in the same shared physical space each day will experience acclimation pains when adjusting to a purely work-from-home environment. If not taken in stride, this friction can cause [serious harm](/handbook/values/#five-dysfunctions) — operationally as well as culturally. 

Remember that transitioning to remote, even if temporary, is a [*process*](/company/culture/all-remote/remote-work-emergency-plan/). You cannot [copy](/company/culture/all-remote/what-not-to-do/) an in-office environment and paste it into a remote one and expect everyone to function as usual. 

It's important to overcommunicate with your team as you adjust. Speak up about issues. Offer solutions for communication gaps. Seek advice on how others have carved out dedicated places of work within their home. Crowdsource advice from within your organization. Look for opportunity in the midst of what will likely feel like a chaotic and destabilized situation. Remote is a chance to rethink how you live and work, and though it may sound counterintuitive, unleashing your imagination to take advantage of your new working reality may lead to long-term efficiencies. 

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/R0AB8ZvnEIU" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

*In the [video](https://youtu.be/IU2nTj6NSlQ) above, GitLab's Head of Remote shares his top tips for employees and employers who have been forced into a work-from-home scenario with [Jack Altman](https://twitter.com/jaltma), CEO of [Lattice](http://lattice.com/). Discover more in GitLab's [Remote Work playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0Kq7QUX-Ux5fOunQotqJbECc).*

## <%= partial("company/culture/all-remote/coursera_gitlab_remote_course.erb") %>

## Additional resources

We recognize that many workers are in need of establishing baseline remote principles right away. For those who wish to dive deeper, consider studying and implementing the guides below — surfaced from GitLab's [comprehensive guide to remote work](http://allremote.info/). 

- [How to live your best remote life](/blog/2019/07/09/tips-for-working-from-home-remote-work/)

- [GitLab's guide for starting a remote job](/company/culture/all-remote/getting-started/)

- [Pitfalls to watch out for when embracing remote](/company/culture/all-remote/what-not-to-do/)

- [Mastering the all-remote environment: My top 5 challenges and solutions](/blog/2019/12/30/mastering-the-all-remote-environment/)

- [5 Tips for mastering video calls](/blog/2019/08/05/tips-for-mastering-video-calls/)
- [Mastering the at-home office environment](/blog/2019/09/12/not-everyone-has-a-home-office/)
- [How to make your home a space that works with kids](/blog/2019/08/01/working-remotely-with-children-at-home/)
- [5 Things to keep in mind while working remotely with kids](/blog/2019/08/08/remote-kids-part-four/)
- [Combating burnout, isolation, and anxiety](/company/culture/all-remote/mental-health/)
- [Adopting a self-service mindset](/company/culture/all-remote/self-service/)
- [Remote Without Warning Webinar: How to adapt and thrive as a suddenly-remote company](https://youtu.be/n4ZZaE-XCVs?t=5)
- [Preparing Your Team for Remote Work Webinar](https://youtu.be/9tYEKAFgQQw?t=5)

## <%= partial("company/culture/all-remote/is_this_advice_any_good_remote.erb") %>

## Contribute your lessons

GitLab believes that all-remote is the [future of work](/company/culture/all-remote/vision/), and remote companies have a shared responsibility to show the way for other organizations who are embracing it. If you or your company has an experience that would benefit the greater world, consider creating a [merge request](https://docs.gitlab.com/ee/user/project/merge_requests/) and adding a contribution to this page.

----

Return to the main [all-remote page](/company/culture/all-remote/).
