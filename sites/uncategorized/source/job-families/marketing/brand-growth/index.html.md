---
layout: job_family_page
title: "Brand Growth - Rloes & Responsibilities"
---

## Levels

### Manager, Brand Growth

The Manager, Brand Growth reports to the Director, Brand.

#### Manager, Brand Growth Job Grade

The Brand Manager is a [grade 8](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Manager, Brand Growth Responsibilities

* Identify how our brand is currently positioned in the market and develop a reporting process to document the progress of our brand
* Implement brand measurement systems so the value of our brand can be easily tracked
* Oversee the brand message creation process, as well as make sure the copy on our web properties align with the message
* Develop and execute marketing campaigns aimed at communicating our brand message
* Educate and communicate our brand personality internally and align company around the message
* Develop brand guidelines and communication programs so entities outside of GitLab, can easily understand how to use our brand
* Brand management for international expansion, including production of brand guidelines in collaboration with the Brand team.
* Measure and report on success of brand marketing campaigns
* Oversee merchandise management
* Provide support with brand team process establishment and documentation
* Lead Brand Growth Content team

#### Manager, Brand Growth Requirements

* Ability to use GitLab
* 5-7 years experience in a brand strategy role
* Knowledge of brand tracking and brand measurement systems
* Capability to coordinate across many teams and perform in fast-moving startup environment
* Understand and be able to express the value of GitLab as a brand entity
* Outstanding written and verbal communications skills
* You embrace our values, and work in accordance with those values.
* Develop pre-event, at-event, and post-event marketing plans in conjunction with product marketing, content, alliances, demand generation and field teams.
* Advance, maintain, and manage external partners and vendors for multiple events and swag, including negotiating optimal terms.
* Partner with internal cross-functional teams to create event agendas, campaigns, and content that meet engagement targets.
* Oversee creation of event communications and promotion for event programs, in partnership with demand generation and sales development teams, to drive attendance.
* Develop and deliver event enablement to include pre-event briefings, at-event briefings, and drive post-event follow-up briefings and event retrospectives.

## Performance Indicators

* Executing overall brand strategy and brand campaign initiatives 
* Collaborating effectively and receiving overall buy-in internally 
* Reporting on metrics on brand campaigns to show that our brand is trending upwards. 
 
## Career Ladder

* The next step in the Corporate Events job family is not yet defined at GitLab.

## Hiring process

Candidates for these positions can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our team page.

* Qualified candidates will be invited to schedule a 30 minute screening call with one of our Global Recruiters
* Next, candidates will be invited to schedule a first interview with the Hiring Manager
* Next, candidates will be invited to interview with 2-5 team members
* There may be a final executive interview

Additional details about our process can be found on our [hiring page](/handbook/hiring/).
