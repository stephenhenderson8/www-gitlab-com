---
layout: handbook-page-toc
title: Business Technology
description: Business Technology
---

<link rel="stylesheet" type="text/css" href="/stylesheets/biztech.css" />

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## <i id="biz-tech-icons" class="far fa-paper-plane"></i>How to reach out to us?

<div class="flex-row" markdown="0">
  <div>
    <h5>Team Member Enablement</h5>
    <a href="/handbook/business-technology/team-member-enablement/" class="btn btn-purple" style="width:170px;margin:5px;">Handbook page</a>
    <a href="https://gitlab.com/gitlab-com/business-technology/team-member-enablement/issue-tracker/-/issues/new" class="btn btn-purple" style="width:170px;margin:5px;">Issue tracker</a>
  </div>
  <div>
    <h5>Enterprise Applications</h5>
    <a href="/handbook/business-technology/enterprise-applications/" class="btn btn-purple" style="width:170px;margin:5px;">Handbook page</a>
    <a href="https://gitlab.com/gitlab-com/business-technology/Business-Technology/-/issues/new" class="btn btn-purple" style="width:170px;margin:5px;">Issue tracker</a>
  </div>
</div>

<div class="flex-row" markdown="0">
  <div>
    <h5>Procurement</h5>
    <a href="/handbook/finance/procurement/" class="btn btn-purple" style="width:170px;margin:5px;">Handbook page</a>
    <a href="/handbook/finance/procurement/#-contacting-procurement" class="btn btn-purple" style="width:170px;margin:5px;">Contact the team</a>
  </div>
  <div>
    <h5>Data Team</h5>
    <a href="/handbook/business-technology/data-team" class="btn btn-purple" style="width:170px;margin:5px;">Handbook page</a>
    <a href="/handbook/business-technology/data-team/#contact-us" class="btn btn-purple" style="width:170px;margin:5px;">Contact the team</a>
  </div>
</div>

<div class="flex-row" markdown="0">
  <div>
    <h5>IT Compliance</h5>
    <a href="/handbook/business-technology/it-compliance/" class="btn btn-purple" style="width:170px;margin:5px;">Handbook page</a>
    <a href="https://gitlab.com/gitlab-com/business-technology/it-compliance/it-compliance-issue-tracker/-/issues/new" class="btn btn-purple" style="width:170px;margin:5px;">Issue Tracker</a>
  </div>
  <div>
    <h5>Engineering and Infrastructure</h5>
    <a href="/handbook/business-technology/engineering/" class="btn btn-purple" style="width:170px;margin:5px;">Handbook page</a>
    <a href="/handbook/business-technology/engineering/#how-to-engage-us" class="btn btn-purple" style="width:170px;margin:5px;">Contact the team</a>
  </div>
</div>

## <i id="biz-tech-icons" class="far fa-newspaper"></i>What's happening?

- [FY22 OKRs Business Technology](https://gitlab.com/gitlab-com/business-technology/okrs/-/blob/master/README.md)

## <i id="biz-tech-icons" class="fas fa-tasks"></i>Objectives of our organization

<table id="objectives-table-bizops">
  <tr>
    <th>
        <i class="fas fa-users-cog i-bt"></i>
        <h5>Employee Productivity</h5>
    </th>
    <th>
        <i class="fas fa-user-clock i-bt"></i>
        <h5>Project Delivery & Business Outcomes</h5>
    </th>
    <th>
        <i class="fas fa-chart-line i-bt"></i>
        <h5>Data Driven Business </h5>
    </th>
    <th>
        <i class="fas fa-shield-alt i-bt"></i>
        <h5>Security</h5>
    </th>
  </tr>
  <tr>
      <td>
        <ul>
            <li>Best in class employee experience by removing friction with technology</li>
            <li>Provide employee support model to maximize productivity</li>
            <li>Improve team on-boarding with automated orchestration</li>
            <li>Provide innovative solutions to promote the Remote only business model</li>
        </ul>
      </td>
      <td>
        <ul>
            <li>Stand up Enterprise Applications function to manage and support Tier 1 tech stacks</li>
            <li>Align business priorities and IT roadmaps</li>
            <li>Create Program Management Maturity Model for IT</li>
            <li>Develop nimble IT project methodology to ensure health project delivery</li>
            <li>Develop Enterprise Architecture methodology</li>
        </ul>
      </td>
      <td>
        <ul>
            <li>Create best in class data warehouse to make data driven decisions</li>
            <li>Scale Data Engineering to improve quality of data for high fidelity reporting</li>
            <li>Mature Data Analytics organization as a hybrid model of centralized technical data analysts and distributed business data analysts</li>
        </ul>
      </td>
      <td>
        <ul>
            <li>Support Security Program to meet GitLab customer expectations</li>
            <li>Implement programs and systems to manage corporate assets and data</li>
            <li>Gain alignment and develop cross-functional operating model between Security and IT teams</li>
            <li>Develop IT Compliance program, framework, policies and audit controls</li>
        </ul>
      </td>
  </tr>
</table>

## <i id="biz-tech-icons" class="fas fa-users"></i>The team

### Reaching the Teams

- [How to work with Business Technology](/handbook/business-technology/how-we-work/)
- Groups in GitLab
    - `@gitlab-com/business-technology`
    - `@gitlab-com/business-technology/enterprise-apps/bsa`
    - `@gitlab-com/business-technology/enterprise-apps/financeops`
    - `@gitlab-com/business-technology/team-member-enablement`
    - `@gitlab-com/Finance-Division/procurement-team`
    - `@gitlab-com/business-technology/enterprise-apps/integrations`
- Channels in Slack
    - [`#business-technology`](https://gitlab.slack.com/archives/C01BLS12V37)
    - [`#bt-business-engagement-team`](https://gitlab.slack.com/archives/CKEFG8CBV)
    - [`#bt-engineering`](https://gitlab.slack.com/archives/C01UDK81DRS)
    - [`#bt-finance-operations`](https://gitlab.slack.com/archives/CSTMYD5E1)
    - [`#bt-integrations`](https://gitlab.slack.com/archives/C015U7R5XJ8)
    - [`#infra-access-requests`](https://gitlab.slack.com/archives/C027UCXNCTX)
    - [`#it_help`](https://gitlab.slack.com/archives/CK4EQH50E)
    - [`#procurement`](https://gitlab.slack.com/archives/CPTMP6ZCK)

## Team Building

Strong teams are important in business. Business Technology has incorporated a few [team-building exercises](/handbook/business-technology/team-building/) as an ongoing element of our culture. As a department, we aim to celebrate team spirit, collaboration fun and motivation.

## Documentation

For reference, "[The Documentation System](https://documentation.divio.com/)"

- Tutorials
- How-To Guides
- Explanation
- **Reference** (most of the information you can find in our handbook pages)
