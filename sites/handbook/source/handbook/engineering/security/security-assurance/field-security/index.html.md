---
layout: handbook-page-toc
title: "Field Security Team"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}
 
- TOC
{:toc .hidden-md .hidden-lg}

## <i class="fas fa-bullseye" style="color:rgb(110,73,203)" aria-hidden="true"></i> Field Security Mission
 
The Field Security team serves as the public representation of GitLab's internal Security function. The team is tasked with providing high levels of security assurance to internal and external customers. We work with all GitLab departments to document requests, analyze the risks associated with those requests, and provide value-added remediation recommendations.

Hear more about our team and how we support GitLab in this [interview with the Manager of Field Security.](https://www.youtube.com/watch?v=h95ddzEsTog). 
 
## <i class="far fa-lightbulb" style="color:rgb(110,73,203)" aria-hidden="true"></i> Core Compentencies

In support of GitLab’s Sales and Customer Success Teams, the Field Security team maintains the [Customer Assurance Activities Procedure](/handbook/engineering/security/security-assurance/field-security/customer-security-assessment-process.html) for the intake, tracking, and responding to GitLab Customer and Prospect Security Assurance Activity request. This includes, but is not limited to: 
 
- Proactively maintaining self-service security and privacy resrouces including the [Customer Assurance Package](https://about.gitlab.com/security/cap/), the [Trust Center](https://about.gitlab.com/security/) and the **internal only** [Answer Base](https://about.gitlab.com/handbook/engineering/security/security-assurance/field-security/answerbase.html).
- Completing Security, Privacy, and Risk Management Questionnaires
- Assisting in Contract Reviews
- Participating in Customer Meetings
- Providing External Evidence (such as GitLab’s SOC2 Penetration Test reports)
- Conducting Annual [Field Security Study](https://about.gitlab.com/handbook/engineering/security/security-assurance/field-security/field-security-study.html)
 
Requests for Customer Assurance Activities should be submitted using the **Customer Assurance** workflow in the `#sec-fieldsecurity` Slack Channel. Detailed work instructions are located in the [Field Security Project](https://gitlab.com/gitlab-com/gl-security/security-assurance/field-security-team/field-security#customer-assurance-activities). 

### Feedback and the Future of Field Security

Do you have an idea, feedback, or recommendation for how Field Security can better support you? Pleaes open a General Request issue in the [Field Security Project.](https://gitlab.com/gitlab-com/gl-security/security-assurance/field-security-team/field-security) 
 
<!-- blank line -->
----
<!-- blank line -->

## <i id="biz-tech-icons" class="fas fa-tasks"></i>Metrics and Measures of Success

[Security Impact on Net ARR](https://about.gitlab.com/handbook/engineering/security/performance-indicators/#security-impact-on-net-arr)

## <i class="fas fa-id-card" style="color:rgb(110,73,203)" aria-hidden="true"></i> Contact the Team

|Team Member|Role|Responsibilities|
|:----------:|:----------:|:---------------:|
|[Marie-Claire Cerny](https://about.gitlab.com/company/team/#marieclairecerny) @marieclairecerny  | Field Security Engineer| [Customer Assurance Activities DRI](/handbook/engineering/security/security-assurance/field-security/customer-security-assessment-process.html) |
| TBD | Senior Field Security Engineer (US Federal) | US Federal and FedRAMP |
|[Meghan Maneval](https://about.gitlab.com/company/team/?department=security-department#mmaneval20) | Manager, Field Security | |

* GitLab Issues
  * `/gitlab-com/gl-security/security-assurance/field-security-team`
* Email
  * `security-assurance@gitlab.com`
* Slack
  * Group Handle
    * `@field-security`
  * Primary Slack Channels
    * `#sec-fieldsecurity`
    * `#sec-assurance`

## <i class="fas fa-book" style="color:rgb(110,73,203)" aria-hidden="true"></i> References

* [Customer Assurance Activities Procedure](/handbook/engineering/security/security-assurance/field-security/customer-security-assessment-process.html)
* [Customer Assurance Pacakge](https://about.gitlab.com/security/cap/)
* [AnswerBase](https://about.gitlab.com/handbook/engineering/security/security-assurance/field-security/answerbase.html)
* [Field Security Study](https://about.gitlab.com/handbook/engineering/security/security-assurance/field-security/field-security-study.html)
* [Independent Security Assurance](https://about.gitlab.com/handbook/engineering/security/security-assurance/field-security/independent_security_assurance.html)
* [Security Shadow Program](https://about.gitlab.com/handbook/engineering/security/security-shadow-security-assurance.html)

<div class="flex-row" markdown="0" style="height:40px">
    <a href="https://about.gitlab.com/handbook/engineering/security/security-assurance/security-assurance.html#" class="btn btn-purple-inv" style="width:100%;height:100%;margin:1px;display:flex;justify-content:center;align-items:center;">Return to the Security Assurance Homepage</a>
</div> 
