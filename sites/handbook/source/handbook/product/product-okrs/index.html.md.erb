---
layout: handbook-page-toc
title: Product Organization OKRs
---
## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

<%= partial("handbook/product/product-handbook-links") %>

## What are OKRs

OKRs (Objectives and Key Results) are a simple goal setting technique propelled by Google and implemented by many leading companies with astounding success. [Five key OKR benefits](https://www.whatmatters.com/faqs/benefits-of-okrs/) include: Focus, Alignment, Accountability, Status and Ambition.

John Doerr is a leader in the OKR world and authored the book Measure What Matters, a handbook for setting and achieving audacious goals. He as since built a company called What Matters, that gives leaders the tools they need to achieve their goals. The [What Matters website](https://www.whatmatters.com/) is a rich and detailed resource for OKRs. You can also check out the [how to write OKRs](#how-to-write-okrs) section of this page for more guidance.

## Why we do Product OKRs

Product Management has the potential to drive and influence growth as well as company goal attainment in ways that other organizations in a company [cannot](https://productled.com/blog/product-led-growth-definition/).
When we consider our [Objectives and Key Results (OKRs)](https://about.gitlab.com/company/okrs/#what-are-okrs) across the Product, Engineering, UX, and Quality organizations, we can do more to make them connected and concerted. One of the [key implications for alignment across teams](https://www.forbes.com/sites/forbescommunicationscouncil/2018/12/19/three-implications-of-product-led-growth/?sh=77ccb6635fc5) is that you share goals and KPIs.
This enables us to be product-led by focusing on [Essentialism](https://about.gitlab.com/handbook/product/product-principles/#our-product-principles) to drive results that matter most for the customer across the aligned product groups.
As it is written in the handbook, [GitLab's OKRs](https://about.gitlab.com/company/okrs/#what-are-okrs) are meant to be the "how to achieve the goal" of a KPI. So, the question we need to consider is are we optimizing our goals to achieve the KPI in the best way?

## GitLab OKRs

GitLab uses the [OKR (Objective and Key Results) framework](https://www.youtube.com/watch?v=L4N1q4RNi9I) to set and track goals on a quarterly basis. You can read more about OKRs at GitLab OKRs including the overall process as well as current and past OKRs [here](https://about.gitlab.com/company/okrs/). Every department participates in OKRs to collaboratively goal set across the organization. GitLab does quarterly objectives which align with our Fiscal Year.

## Product OKRs

Through FY22-Q2, Product team OKRs will be tracked as Epics [gitlab.com](https://gitlab.com/groups/gitlab-com/-/epics?scope=all&state=opened&search=FY22-Q2+OKR) and the KRs are tracked as Issues in the [Product project](https://gitlab.com/gitlab-com/Product) and on the [Product OKR Board](https://gitlab.com/gitlab-com/Product/-/boards/906048?label_name%5B%5D=OKR). Starting in FY22-Q2, all OKRs are also in [Ally.io](https://app.ally.io/teams/39480/objectives?tab=0&time_period_id=135090) so  we can get familiar with the tool before FY22-Q3. Starting in FY22-Q3, we're aiming to replace use of GitLab epics, issues and boards and fully port all OKRs into Ally.io. Details on that process and instructions on how to participate will be added to this page as they solidify.

To learn more about the rollout process for Product and Engineering see the linked [Ally OKR Rollout GitLab Project](https://gitlab.com/gitlab-com/ally-okr-rollout).

Each organization at GitLab may follow a slightly personalized process for OKRs. Below is the process for Product. Product works closely with Engineering and collaborates on some of the same Objectives and Key Results. You can reference the Engineering's process [here](https://about.gitlab.com/handbook/engineering/#engineering-okr-process). One important thing to not is that the Engineering team leverages indivdiual OKRs more broadly than the product team. To manage scope, each engineering team member  is limited to no more tha 9 committed KRs per quarter.

The Product OKRs are initiated by the Chief Product Officer each quarter per the details and timeline below.

## Shared R&D OKRs

Shared R&D OKRs stem from Objectives the CTO and CProdO align on, jointly with their leadership teams. These OKRs are not function-specific, as they are identified as a necessary priority across the whole product ecosystem by the CTO and CProdO and approved by the CEO. Shared R&D Objectives may be owned by either the CTO or the CProdO, and the KRs will be cascaded to cross-functional team members regardless of reporting/organizational structure. For example, the VP of User Experience may own a KR that cascades from an Objective owned by the CProdO. Shared R&D OKRs will be worked on as a priority by [Product Groups](/handbook/product/product-processes/#quad--infra--security-product-group-dris).

### FY22 Q4 Product OKR Process

**Below is our Q4 iteration on the OKR process.** This Q4 process/timeline incorporates [FY22 Q3 OKR pilot learnings](https://gitlab.com/gitlab-com/Product/-/issues/2714). To address some of the findings, we're introducing **Shared R&D OKRs** to clarify and highlight Objectives aligned upon by the CProdO and the CTO. The Shared R&D OKRs will then serve as the priority across the Product and Engineering teams.

- Quarterly priority OKRs from CTO and CprodO will be referred to as [**Shared R&D OKRs**](#shared-rd-okrs)
- The teams responsible for collaborating on these OKRs will be referred to as [**Product Groups**](/handbook/product/product-processes/#quad--infra--security-product-group-dris)


For guidance about the Product OKR process, team members can post in Slack #product and tag [Product Operations](https://about.gitlab.com/company/team/#fseifoddini). Team members can also attend [Product Operations office hours](/handbook/product/product-processes/#product-operations-office-hours-bi-weekly) for live support. For guidance on how to use Ally.io please check out [how to use ally.io](#how-to-use-allyio) or post in Slack #ally-okr-rollout and tag Product Operations. 

Please share your feedback in [Feedback on Q4 Shared R&D OKR Process](https://gitlab.com/gitlab-com/Product/-/issues/3142)

#### OKR Kick-off by the Product Leadership Team

- **6 weeks** prior to the start of the fiscal quarter, the CProdO and CTO begin alignment on potential Shared R&D OKRs to recommend to the CEO. The EBA to the CTO will schedule these meetings and include cross-functional team members as directed by by the CTO: 
     - 2021/09/20: Shared R&D Objectives for OKRs
          - Goal: propose cross-functional OKR ideas for CEO’s top-down OKR draft
          - [Agenda](https://docs.google.com/document/d/1IGH5Kv8MnYNoOEFRp7x_dOmpWZJz82doAV9CPv8zASk/edit#)
     - 2021/09/22: Discuss Shared R&D OKRs
          - Goal: CProdO and CTO review align on top OKR ideas
     - 2021/09/27: Shared R&D Objectives for OKRs
          - Goal: finalize cross-functional OKR ideas for CEO's top-down OKR draft
          - [Agenda](https://docs.google.com/document/d/1IGH5Kv8MnYNoOEFRp7x_dOmpWZJz82doAV9CPv8zASk/edit#)
- **5 weeks** prior to the start of the fiscal quarter, the CEO and Chief of Staff initiate the OKR process. The CEO will share an MR in the #e-group Slack channel with the 3 Company Objectives for the quarter, and one to three Key Results for each objective. 
-  **4 weeks** prior to the start of the new quarter:
     - the CProdO adds the aligned upon Shared R&D OKRs plus any additional Product organization specific OKRs to [FY22-Q4 Shared R&D OKR and Product Org OKR Planning](https://gitlab.com/gitlab-com/ally-okr-rollout/-/issues/36) and reviews the proposed OKRs with the CEO
     - Product Operations informs Product team members that Q4 OKR planning has begun by tagging them into this issue, highlighting that it is recommended for Product Managers to review and activate their [Product Group](https://about.gitlab.com/handbook/product/product-processes/#quad--infra--security-product-group-dris) to begin ideation on their Q4 KRs at this time. 

**Notes:** 

- The CTO drafts any Engineering organization specific OKRs per the [engineering OKR process](https://about.gitlab.com/handbook/engineering/#engineering-okr-process) 
- The CProdO and CTO will collaborate such that the total number of Shared R&D, Product team specific, and Engineering team specific objectives do not exceed a total of six across Engineering and Product organizations


#### OKR Collaboration by the Rest of the Product Team

- **3 weeks** prior to the start of the new quarter, the CEO approves the Shared R&D OKRs and Product organization specific OKRs. The CProdO updates both the  Shared R&D and Product organization specific OKRs in [FY22-Q4 Shared R & D OKR Planning](https://gitlab.com/gitlab-com/ally-okr-rollout/-/issues/36) as appropriate and tags in the teams for alignment and to finalize ownership of the KRs
- **2 weeks** prior to the start of the new quarter:
     - Product Operations ports the finalized Shared R&D and Product organization OKRs approved by the CProdO, CTO and VP of Product from the planning issue  into Ally.io. Product Operations assigns the Objectives and Key Results to the designated owners, as specified by in the issues
     - Product Operations  aligns the Shared R&D OKRs and Product organization OKRs to their corresponding parent-level OKRs in Ally.io so everything cascades from the Company Objectives set by the CEO and Chief of Staff. 
     - Product Operations notifies Product Directors and Group Managers of Product that the approved Shared R&D and Product organization OKRs are in Ally.io, requesting they review and activate their Product Managers to work with their Product Group to add any additional KRs they'd like to contribute.
     - Product Managers initiate the planning process. They will work with their Product Group to identify KRs supporting Shared R&D Objectives owned by the CProdO and CTO, as this is the agreed-upon priority across the Product and  Engineering teams.
     - Product Managers may, in alignment and partnership with their Product Group, choose to create KRs not aligned to existing Shared R&D OKRs. In this case they may either:
          - Define Key Results for another existing Q4 Product or Engineering specific OKR in Ally.io.
          - Identify a unique Objective and then define the Key Results (connect with Product Operations on where to add in Ally.io).
          - Define Key Results for one of the [yearly Product themes](https://about.gitlab.com/direction/#fy22-product-investment-themes) not yet utilized as an objective in Q4 (connect with Product Operations on where to add in Ally.io).
     - Product Managers will add any additional KRs to Ally.io
          - For guidance, please check out [how to use ally.io](#how-to-use-allyio): [Ally/OKR overview video for Product Managers](https://www.youtube.com/watch?v=M7z6ZnEpztU) and [how OKR alignments work in Ally](https://www.youtube.com/watch?v=pRl5QkvJEq8)
     - In some cases, GMPs may want to act as KR owners, working with the Product Manager and Product Group to add additional KRs to Ally.io.
          - For guidance, please check out [how to use ally.io](#how-to-use-allyio): [Ally/OKR overview video for GMPs](https://youtu.be/ilx7lk-_VM8) and [how OKR alignments work in Ally](https://www.youtube.com/watch?v=pRl5QkvJEq8)
- **1 week** prior to the start of the new quarter:
     - the CProdO and CTO record a video highligthing and discussing the Shared R&D OKRs
          - Product Operations shares the video across relevant Slack channels
     - Product Directors review any additional OKRs added to Ally.io by GMPs and Product Managers

**Important note on when Product Manager participation in OKRs**

Product Managers may be pulled in for collaboration throughout the OKR drafting process. Regardless of whether a Product Manager is a KR owner or not, they may need to work with their manager and [Product Group](https://about.gitlab.com/handbook/product/product-processes/#quad--infra--security-product-group-dris) to plan and execute work to support OKRs throughout the quarter.


#### Updating the Status and Progress of OKRs 

**When to update the status of Product OKRs** 

- For all Product team OKRs, the KR owners are responsible for keeping the status of their KRs in Ally.io up to date
     - Team members who **own** specific OKRs will update [the score (% complete) in Ally.io](https://about.gitlab.com/company/okrs/#scoring-okrs) monthly and ping any relevant stakeholders as needed. For example, since FY22-Q4 begins November 1, KR owners will provide score updates by   November 15,  December 15 and January 15. We follow this mid-month cycle to ensure there are accurate OKR status updates for [Key Reviews](https://about.gitlab.com/handbook/key-review/) which typically happen around the 20th of each month.
- **Owners** of KRs will get pinged with reminders to do "check-ins" via  Ally/Slack around the 5th of each month. To get more reminders or to opt-out, visit [how to use Ally.io](#how-to-use-allyio) 
- **Owners** of KRs will also get pinged in the [Product Key Review issue](https://gitlab.com/gitlab-com/Product/-/issues/2919) as needed to update the Product Key Review slides around the 18th of each month based on what is in Ally.io

**How to report progress (% score in Ally) of Product OKRs**

In order to maintain consistency in evaluating progress across R & D, all Product OKRs will follow the scoring method outlined below as proposed in [Scoring Shared OKRs Consistently Across R & D](https://gitlab.com/gitlab-com/ally-okr-rollout/-/issues/25).

At the beginning of each quarter, shared product KR owners should lead and align the [Product Group](https://about.gitlab.com/handbook/product/product-processes/#quad--infra--security-product-group-dris) (the cross-functional team doing the work) to define a plan to accomplish their KRs as scorable in the following manner:

**Scoring Method: Scale**

This method allows for flexibility and can give you credit for achieving part of the key result. KRs are scored on a 0-100% scale. If you deliver part of the KR you can credit that result as a % score. 

     Example:

     Objective: Improve License Management

     - KR1 - Close 16 License issues
     - KR2 - Reduce license-related support tickets by 4%
     - KR3 - Manually audit 100% of missing license data

     If you use the scale scoring method it would look like this:

     - KR1 - Closed 8 of 16 issues - 50%
     - KR2 - Reduced license-related support tickets by 3.8% - 95%
     - KR3 - Manually audited 100% of missing license data - 100%

In this scenario you didn’t complete two of the KRs, but since you selected the Scale method you were able to get credit for what was completed. For example, 8 of 16 pain points is 1/2 of the KR, so the score is 50%. If additional issues were found during development, you would create a new OKR to resolve those issues the following quarter rather than shift the target of your current quarter.

**Note:** It is recommended to leverage the [Ally/GitLab issue integration](https://www.youtube.com/watch?v=cLgrylX8ufw) to help automate the % tracking of the progress of KRs in Ally in tandem with the methodology above. This will create a clear tie between KRs in Ally and the work related to the KR in GitLab. However, KRs can also be scored manually in Ally.io. For more guidance on how to leverage the Ally/GitLab issue integration see this [sample issue](https://gitlab.com/gitlab-com/ally-okr-rollout/-/issues/6#examples) and other resources in [how to use Allio.io](#how-to-use-allyio).

**How this will look in Product Key Review slides**

- Product Key Reviews will reference Ally.io in an [OKR specific slide](https://docs.google.com/presentation/d/1O5Kc1kpJJBm5aJFT24V05xuR3pHjCGzID-QkYcUSZ5k/edit#slide=id.ge7a2c8f4b9_0_12)
- Product team OKR owners may leverage this [slide template](https://docs.google.com/presentation/d/1O5Kc1kpJJBm5aJFT24V05xuR3pHjCGzID-QkYcUSZ5k/edit#slide=id.geb1b1c60d1_1_4) if additional details are needed for their KRs


### FY22 Q3 Product OKR Process

_In FY22 Q3/Q4 we'll have an updated process to fully leverage Ally.io as SSOT for OKRs and sunset usage of GitLab epics/issues. Details forthcoming._

_During FY22 Q3, we expect many questions as we pilot Allied Product Group OKRs and the adoption of Ally.io. Any time Product Managers or any other team members need live support, they should add themselves to the agenda and attend [Product Operations office hours](https://about.gitlab.com/handbook/product/product-processes/#product-operations-office-hours-bi-weekly)._

#### KR Creation by Product Groups (referred to as Shared Product Group OKRs)

**Context:** <br>
The Q3 OKR process outlined below is a follow-up to the [Q2 OKR experiment](https://gitlab.com/gitlab-com/Product/-/issues/2095) and designed to pilot **Shared Product Group OKRs**. Shared Product Group OKRs are encouraged to be drafted against GitLab's [yearly product themes](https://about.gitlab.com/direction/#fy22-product-investment-themes) and focus on driving ground-up motion to inform the business and deliver value to customers. They may, however, also be drafted outside of a product theme, targeting a Stage- or Group-level KPI for example, if the cross-functional team aligns and chooses to do so. Product management will collaborate closely with the [Product Group](https://about.gitlab.com/handbook/product/product-processes/#quad--infra--security-product-group-dris) (Engineering, UX, and Quality, as well as Infra and Security, when needed) to create Shared Product Group OKRs. This approach aims to get ahead of the executive OKR process and provide the e-group with our opinionated view of what we plan to do. The Product Group will collectively create shared key results at the Product Group level. Three key benefits of this approach:

1. Product Managers serve as Shared Product Group DRIs to ensure OKRs align with the vision of the product group
1. We further empower all Product Group [DRIs](https://about.gitlab.com/handbook/people-group/directly-responsible-individuals/) to use existing prioritization systems to accomplish these targets, rather than this being a top-down initiative
1. The Product Group has the most context on the customer, the technology, the quality, performance, and usability of the product to come up with a goal that can be successfully delivered to move the needle for the business

We realize the introduction of Shared Product Group OKRs may initially cause some perceived or actual conflicts with other team members' functional team OKRs. By first doing this as an optional pilot for product groups, rather than a full roll-out, we hope to capture these challenges and work through them. Please surface any challenges to your manager, in [Product Operations office hours](https://about.gitlab.com/handbook/product/product-processes/#product-operations-office-hours-bi-weekly) or in [Feedback on Q3 Product Group Planning](https://gitlab.com/gitlab-com/Product/-/issues/2714).

**Guiding themes for Shared Product Group OKRs:**
- [Application Security Testing](https://about.gitlab.com/direction/#application-security-testing-leadership)
- [Adoption Through Usability](https://about.gitlab.com/direction/#adoption-through-usability)
- [SaaS First](https://about.gitlab.com/direction/#saas-first)


- **8 weeks** prior to the start of the new quarter, [the EBA to the Chief Product Officer](https://about.gitlab.com/company/team/#kristie.thomas) will create the planning Epic and Issues for Product Org and Group OKRs. These will be used as a place for teams suggest and draft their proposed OKRs in the issue comments.
  - The [EBA to the Chief Product Officer](https://about.gitlab.com/company/team/#kristie.thomas) will create [FY22-Q3 Planning Epic](https://gitlab.com/groups/gitlab-com/-/epics/1509) containing three separate issues for each 1-year product theme, and a fourth issue for OKRs not aligned with a theme. EBA will tag in [Product Operations](https://about.gitlab.com/company/team/#fseifoddini). The EBA will also create the Product Org OKR Issue which is driven by the Chief Product Officer, and nested under the above referenced FY22-Q3 Planning Epic as well.  
     - Product Operations will review and then tag all first line managers (Group Managers, or Directors) requesting they activate their Product Managers to propose product group KRs corresponding to one or more of the 1-year product themes as Objectives. 
     - GMPs will organize review and approval of their team's KRs at their discretion but can view suggestions [here](#how-to-write-OKRs). We strongly recommend aligning KRs to at least one of the known FY22 product themes, however, it is not a hard requirement. Please note if a team is not aligning KRs to an existing product theme as an Objective, they'll have to first define an Objective and then define the Key Results.
     - Product Managers are the DRI of Shared Product Group OKRs. As such, they will own the planning and definition in partnership with their [Product Group  (Product Manager, Engineering Manager, Product Designer, Quality Engineer)](https://about.gitlab.com/handbook/product/product-processes/#quad--infra--security-product-group-dris) and can also include their Infrastructure and Security counterparts, if applicable to the objectives.  
     - Once the Shared Product Group KRs are reviewed and approved by their managers, PMs will add their aligned upon KRs to the description of the corresponding 1-year product theme issue or the no-theme affiliated issue in the [Fy22-Q3 OKR Planning Epic](https://gitlab.com/groups/gitlab-com/-/epics/1509) When adding KRs to the issue description the following format should be used: 
        - Section Name
        - Group Name
        - List of Key Results
     - Product Managers will link any related KR epics or issues as appropriate to the corresponding 1-year product theme issue or the no-theme affiliated issue as they become available for tracking. 

- **6 weeks** prior to the start of the new quarter, GMPs have their manager review their team's KRs from the planning issues which are nested under the [Fy22-Q3 OKR Planning Epic](https://gitlab.com/groups/gitlab-com/-/epics/1509).
     - Product operations reviews issues/KRs and may ping GMPs and PMs with questions or reminders. At this time, Product operations will also alert the Chief Product Officer and VP Product Management that the KRs are read to be leverage for Product Organanization OKR planning.

     **IMPORTANT NOTES:**

     - As DRI of Shared Product Group OKRs, Product Managers own the SSOT of the KRs, be they housed in GitLab epics/issues or in Ally.io. If duplicate drafts are found anywhere, it should be flagged in [Feedback on Q3 Product Group Planning](https://gitlab.com/gitlab-com/Product/-/issues/2714), tagging Product Operations `@fseifoddini` and Quality `@meks` for resolution.

     - For guidance on drafting clear and achievable OKRs check out [how to write OKRs](#how-to-write-okrs)

#### OKR Kick-off by the Product Leadership Team

- **5 weeks** prior to the start of the fiscal quarter, the CEO and Chief of Staff initiate the OKR process. The CEO will share an MR in the #e-group Slack channel with the 3 Company Objectives for the quarter, and one to three Key Results for each objective.

     - The Chief Product Officer and the VP of Product will review the Group Product OKRs, located in the [1-year theme OKR issues](https://gitlab.com/groups/gitlab-com/-/epics/1509) as they prepare to [draft their Q3 OKRs](https://gitlab.com/gitlab-com/Product/-/issues/2715).

-  **4 weeks** prior to the start of the new quarter the Chief Product Officer will draft their OKRs in the [Product Org FY22-Q3 Planning Issue](https://gitlab.com/gitlab-com/Product/-/issues/2715) in the [Product Project](https://gitlab.com/gitlab-com/Product/-/issues) and reviews the drafted Product OKRs with the CEO.

- **3 weeks** prior to the start of the new quarter, the CEO approves the OKRs and the Chief Product Officer shares the OKRs with the [product leadership](/handbook/product/product-leadership/) team for alignment and to finalize ownership of the KRs to specific product team members.
- **2 weeks** prior to the start of the new quarter, the EBA will port the finalized Chief Product Officer OKRs and KRs from [the Q3 planning issue](https://gitlab.com/gitlab-com/Product/-/issues/2715) approved by Chief Product Officer and VP of Product into [Ally.io](https://app.ally.io/teams/39480/objectives?tab=0&time_period_id=135090). EBA will assign the Objectives and Key Results to the designated owners, as specified by the Chief Product Officer.
     - The EBA will align the Chief Product Officer OKRs to their corresponding parent-level OKRs so everything cascades from the Company Objectives set by the CEO and Chief of Staff.
     - The EBA will link the OKR descriptions in Ally.io to the related GitLab epics/issues for ease of reference to the planning artifacts.
- **1 week** prior to the start of the new quarter, the EBA will tag Product Operations and GMPs to notify them the approved [Product Org Objectives and Key Results](https://gitlab.com/gitlab-com/Product/-/issues/2715) are in Ally. GMPs then do a final review  of and add any additional OKRs from their team to Ally that are not already included in the approved Product Org OKRs in Ally. **We request that GMPs ensure any OKRs they add do not already exist in Ally.**
     - To learn how GMPs can add their Producut Groups' Shared Product OKRs to Ally, please watch [this video](https://youtu.be/ilx7lk-_VM8), which can also be found in [how to use Ally.io](#how-to-use-allyio).

#### OKR Collaboration by the rest of Product Team

**How and when Product Managers can add additional OKRs**

Product Managers will be pulled in for collaboration throughout the OKR drafting process. Regardless of whether a Product Manager is a KR owner or not, they will need to work with their [Product Group](https://about.gitlab.com/handbook/product/product-processes/#quad--infra--security-product-group-dris) to plan and execute work to support product OKRs beginning around 2 weeks prior to the start of the new quarter for which the OKR is assigned. For an overview of how to review or add OKRs and how to plan/track the work, check out [how to use ally.io](#how-to-use-allyio), as well as this [overview video for Product Managers](https://www.youtube.com/watch?v=M7z6ZnEpztU) and this [video explaining how OKR alignments work in Ally](https://www.youtube.com/watch?v=pRl5QkvJEq8). 


**When to update the status of Product OKRs**

- For all Product team OKR, the KR owners are responsible for keeping the status. of their KRs in Ally.io up to date
     - Team members who **own** specific OKRs will update [the score (% complete) in Ally.io](https://about.gitlab.com/company/okrs/#scoring-okrs) monthly and ping any relevant stakeholders as needed. For example, since FY22-Q3 begins August 1, KR owners will provide score updates by   September 15,  October 15 and November 15. We follow this mid-month cycle to ensure there are accurate OKR status updates for [Key Reviews](https://about.gitlab.com/handbook/key-review/) which typically happen around the 20th of each month.
- **Owners** of the specific KRs will get pinged with reminders to do "check-ins" via  Ally/Slack. They will also get pinged in the [Product Key Review issue](https://gitlab.com/gitlab-com/Product/-/issues/2919) as needed to update the Product Key Review slides around the 18th of each month based on what is in Ally.io

**Note:** OKR owners will get reminders via Slack/Ally.io on or near the 5th of each month to do status updates. To set up more reminders or opt out of reminders, check out [how to use Ally.io](#how-to-use-allyio)

**How to report progress (% score in Ally) of Product OKRs**

In order to maintain consistency in evaluating progress across R & D, all Product OKRs will follow the scoring method outlined below as proposed in [Scoring Shared OKRs Consistently Across R & D](https://gitlab.com/gitlab-com/ally-okr-rollout/-/issues/25).

At the beginning of each quarter, shared product KR owners should lead and align the [Product Group](https://about.gitlab.com/handbook/product/product-processes/#quad--infra--security-product-group-dris) (the cross-functional team doing the work) to define a plan to accomplish their KRs as scorable in the following manner:

**Scoring Method: Scale**

This method allows for flexibility and can give you credit for achieving part of the key result. KRs are scored on a 0-100% scale. If you deliver part of the KR you can credit that result as a % score.

Example:

Objective: Improve License Management

- KR1 - Close 16 License issues
- KR2 - Reduce license-related support tickets by 4%
- KR3 - Manually audit 100% of missing license data

If you use the scale scoring method it would look like this:

- KR1 - Closed 8 of 16 issues - 50%
- KR2 - Reduced license-related support tickets by 3.8% - 95%
- KR3 - Manually audited 100% of missing license data - 100%

In this scenario you didn’t complete two of the KRs, but since you selected the Scale method you were able to get credit for what was completed. For example,  8 of 16 pain points is 1/2 of the KR, so the score is 50%. If additional issues were found during development, you would create a new OKR to resolve those issues the following quarter rather than shift the target of your current quarter.

**Note:** It is recommended to leverage the [Ally/GitLab issue integration](https://www.youtube.com/watch?v=cLgrylX8ufw) to help automate the % tracking of the progress of KRs in Ally in tandem with the methodology above. This will create a clear tie between KRs in Ally and the work related to the KR in GitLab. However, KRs can also be scored manually in Ally.io. For more guidance on how to leverage the Ally/GitLab issue integration see this [sample issue](https://gitlab.com/gitlab-com/ally-okr-rollout/-/issues/6#examples) and other resources in [how to use Allio.io](#how-to-use-allyio).


**How this will look in Product Key Review slides**

- Product Key Reviews will reference Ally.io in an [OKR specific slide](https://docs.google.com/presentation/d/1O5Kc1kpJJBm5aJFT24V05xuR3pHjCGzID-QkYcUSZ5k/edit#slide=id.ge7a2c8f4b9_0_12)
- Product team OKR owners may leverage this [slide template](https://docs.google.com/presentation/d/1O5Kc1kpJJBm5aJFT24V05xuR3pHjCGzID-QkYcUSZ5k/edit#slide=id.geb1b1c60d1_1_4) if additional details are needed for their KRs.

### FY22-Q2 Product OKR Process

#### OKR Kick-off by the Product Leadership Team

_Please note that the FY22-Q2 process is a "hybrid" model as we continue to leverage Gitlab epics and issues as SSOT for OKR content but shift to scoring OKRs in Ally.io.  We fully transition into Ally.io in FY22-Q3. Thank you for your patience!_

- **5 weeks** prior to the start of the fiscal quarter, the CEO and Chief of Staff initiate the OKR process. The CEO will share an MR in the #e-group Slack channel with the 3 Company Objectives for the quarter, and one to three Key Results for each objective.
- **4 weeks** prior to the start of the new quarter the Chief Product Officer will draft their OKRs in a GitLab issue in the [Product Project](https://gitlab.com/gitlab-com/Product/-/issues) and review the drafted Product OKRs with the CEO.
- **3 weeks** prior to the start of the new quarter, the CEO approves the OKRs and the Chief Product Officer shares the OKRs with the [product leadership](/handbook/product/product-leadership/) team for alignment and to finalize ownership of the KRs to specific product team members.
- **2 weeks** prior to the start of the new quarter, the EBA to the Chief Product Officer will port the finalized OKRs into [Ally.io](https://app.ally.io/teams/39480/objectives?tab=0&time_period_id=135090) and assign the Objectives and Key Results to the designated owners, as specified by the Chief Product Officer in the finalized Gitlab epics/issues.
     - The EBA will align the Product OKRs to their corresponding parent-level OKRs so everything cascades from the Company Objectives set by the CEO and Chief of Staff.
     - The EBA will link the OKR descriptions in Ally.io to the related GitLab epics/issues for ease of reference to the final SSOT.

#### OKR Collaboration by the rest of Product Team

**Updating the status of Product OKRs**

- Team members who **own** Q2 OKRs will update the score (% complete) in the relevant GitLab epics/issues and [Key Reivew slides](https://docs.google.com/presentation/d/1xAbkJC8EAWV49bYIEyRCT7qXkiPnq4CQXX9VM_93szw/edit#slide=id.p) monthly and notify any relevant stakeholders.
- The EBA to the Chief product Officer will then update the [Q2 scores in Ally.io](https://about.gitlab.com/company/okrs/#scoring-okrs) based on the latest Key Review slides by the end of each month.
     - It is recommended to direct link to the monthly update in the GitLab epic/issue  or Key Review slides in the Ally.io OKR description to maintain a clear reference to the SSOT.

## How to write OKRs

It is highly recommended that product groups limit the number of OKRs they commit to so they have reasonable bandwidth to deliver  on them. PMs should consider either setting 1 OKR that's not related to a product theme **OR** 3 or fewer KRs if they are using the product themes as their objectives.

Here's are some examples of  how GMPs and PMs can use  epics/issues to plan and outline their product group OKRs:

[Create stage](https://gitlab.com/gitlab-org/create-stage/-/issues/12875)
     - [Create: Editor group](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/12044)
     - [Create: Code review group](https://gitlab.com/gitlab-org/create-stage/-/issues/12877)


- Manage stage
     - [Compliance group](https://gitlab.com/gitlab-org/gitlab/-/issues/334921)

[Verify stage](https://gitlab.com/gitlab-org/verify-stage/-/issues/89)

To learn more about what OKRs are and the importance of OKRs at GitLab, see the [company OKR handbook page](https://about.gitlab.com/company/okrs/).

To learn about the industry best practices for OKRs, how setting the right goals can mean the difference between success and failure, and how we can use OKRs to hold our leaders and ourselves accountable, watch [John Doerr's Ted Talk](https://www.youtube.com/watch?v=L4N1q4RNi9I). You can also find more resources in [Product Management Learning & Development](https://about.gitlab.com/handbook/product/product-manager-role/learning-and-development/#-kpis-and-metrics)

For information on [getting started with OKRs](https://learn.ally.io/path/employee/getting-started-with-okrs) and [writing basic okrs](https://learn.ally.io/writing-basic-okrs) see the linked courses from our OKR vendor Ally.

Samples of well written KRs from GitLab team members from the [Q2 product group KR experiment](https://gitlab.com/gitlab-com/Product/-/issues/2095). What makes these great is that they're succinct in scope and have clear metrics to measure success.

**Product theme (Objective)**: Drive a meaningful impact on Usability (Bugs, Infradev, Security)
**KRs (Key Results):**
- group::threat insights: Meet SLAs for all P1 and P2 bugs affecting usability
- group::code review: Reduce mean-time-to-close of S1 + S2 bugs by 50%
- group::editor: Complete 10 usability issues related to our primary categories (Web IDE, Snippets, Wiki)

## How to use Ally.io

For an overview of how Product is currently using Ally.io and how to do some basic tasks, check out this [overview video](https://youtu.be/hP9yk_PSj2k). We also have additional training materials provided by Ally.io and Gitlab in [this issue](https://gitlab.com/gitlab-com/ally-okr-rollout/-/issues/9) We recommend you check out everything in this issue but these are some specific links for common topics:

- [How to access Ally.io](https://gitlab.com/gitlab-com/ally-okr-rollout/-/issues/9#how-to-access-ally)
- [How Ally.io and Slack currently work together](https://gitlab.com/gitlab-com/ally-okr-rollout/-/issues/9#slack-integration)
- [How Ally.io and GitLab currently work together](https://gitlab.com/gitlab-com/ally-okr-rollout/-/issues/9#ally-and-gitlab-integration)
     - [Video: How the Ally/GitLab issue integration works for automatic tracking/scoring](https://www.youtube.com/watch?v=cLgrylX8ufw)
     - [Sample issue for how Ally/GitLab work together](https://gitlab.com/gitlab-com/ally-okr-rollout/-/issues/6#examples)

- [How to change the scoring and do check-ins of an OKR in Ally.io](https://gitlab.com/gitlab-com/ally-okr-rollout/-/issues/9#what-are-check-ins)
- [How to update the various details or descriptions of an OKR in Ally.io](https://gitlab.com/gitlab-com/ally-okr-rollout/-/issues/9#how-to-create-okrs)
- [Video: How GMPs should add their teams' Shared Product KRs to Ally?](https://youtu.be/ilx7lk-_VM8)
- [Video: How and when do Product Managers participate in OKRs definition, planning and tracking?](https://www.youtube.com/watch?v=M7z6ZnEpztU)
- [Video: How OKR alignments work in Ally](https://www.youtube.com/watch?v=pRl5QkvJEq8)


### Product OKRs by Quarter
##### Current Quarter

[View in Ally.io](https://app.ally.io/teams/39480/objectives?tab=0&time_period_id=135092)

<iframe src="https://app.ally.io/public/AlHP9KoxhNsSCtY" class="dashboard-embed" height="4400" width="100%" style="border:none;"> </iframe>

##### Past Quarters
- [Product OKRs FY22-Q2](https://gitlab.com/gitlab-com/Product/-/issues/2376)
- [Product OKRs FY22-Q1](https://gitlab.com/gitlab-com/Product/-/issues/1914)
- [Product OKRs FY21-Q4](https://gitlab.com/gitlab-com/Product/-/issues/1566)
- [Product OKRs FY21-Q3](https://gitlab.com/gitlab-com/Product/-/issues/1320)

## How to contribute to this page

We encourage suggestions and improvements to the Product OKR process so please feel free to raise an MR to this page. To keep us all aligned, as the process touches all teams across the company, please assign all MRs for collaboration, review and approval to [Product Operations](https://gitlab.com/fseifoddini) and the [EBA to the Chief Product Officer](https://gitlab.com/kristie.thomas).
